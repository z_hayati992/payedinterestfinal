package com.dotin.edu.common;

import com.dotin.edu.entity.Deposit;
import com.dotin.edu.entity.DepositType;
import com.dotin.edu.entity.Interest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;


public class Main {
    public static void main(String[] args) {
        Map<Deposit, Double> map = readXml();
        map = compare(map);
        writeToFile(map);


    }

    private static void writeToFile(Map<Deposit, Double> objectList) {
        File file = new File("result.txt");
        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            Set set = objectList.entrySet();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                Map.Entry map = (Map.Entry) iterator.next();
                Deposit deposit = (Deposit) map.getKey();
                output.write(deposit.getCustomerNumber());
                System.out.println(map.getValue());
                output.write("#");
                output.write(Double.toString((Double) map.getValue()));
                output.write("\n");
            }
            /*
            for (Object object : objectList) {
                Method method = object.getClass().getMethod("getCustomerNumber");
                Object customernumber = method.invoke(object);
                Method method1 = object.getClass().getMethod("getInterest");
                Interest interest = (Interest) method1.invoke(object);
                Object interestValue = interest.getInterest();
                output.write(customernumber.toString());

                output.write(interestValue.toString());
                output.write("\n");


            }

             */
            output.close();
        } catch (Exception e) {
            e.getMessage();
        }


    }

    private static Map<Deposit, Double> compare(Map<Deposit, Double> map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }
        /*
        final String attribute = "Interest"; // for example. Also, this is case-sensitive
        Collections.sort(objectList, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                try {
                    Method m = o1.getClass().getMethod("get" + attribute);
                    // Assume String type. If different, you must handle each type
                    Interest interest1 = (Interest) m.invoke(o1);
                    Interest interest2 = (Interest) m.invoke(o2);
                    Double interestValue1 = interest1.getInterest();
                    Double interestValue2 = interest2.getInterest();
                    return interestValue2.compareTo(interestValue1);
                    // simply re-throw checked exceptions wrapped in an unchecked exception
                } catch (SecurityException e) {
                    throw new RuntimeException(e);
                } catch (NoSuchMethodException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        });

         */


    private static Map<Deposit, Double> readXml() {
        Map<Deposit, Double> map = new HashMap<Deposit, Double>();
        String customerNumber = null;
        String depositType = null;
        Double depositBalance = null;
        int durationDays = 0;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File("file.xml"));
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("deposit");
            int temp = 0;
            while (temp < nodeList.getLength()) {
                Node node = nodeList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    customerNumber = element.getElementsByTagName("customerNumber").item(0).getTextContent();
                    depositType = element.getElementsByTagName("depositType").item(0).getTextContent();
                    depositBalance = Double.parseDouble(element.getElementsByTagName("depositBalance").item(0).getTextContent());
                    durationDays = Integer.parseInt(element.getElementsByTagName("durationDays").item(0).getTextContent());
                }

                boolean valid = validate(depositBalance, depositType, durationDays);
                if (valid) {
                    DepositType depositTypeValue = DepositType.convert(depositType);
                    DepositFactory depositFactory = new DepositFactory();
                    Deposit deposit = depositFactory.getDeposit(depositTypeValue, customerNumber, depositBalance, durationDays);
                    Double interest = Interest.CalculatInterest(deposit);
                    map.put(deposit, interest);
                    temp++;


                } else
                    temp++;
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return map;
    }

    private static boolean validate(Double depositBlance, String depositType, int durationDay) {
        boolean valid = true;
        try {
            if (depositBlance < 0) {
                valid = false;
                throw new Exception("depositBalance not valid");

            }
            if (durationDay <= 0) {
                valid = false;
                throw new Exception("durationDays not valid");

            }
            if (!checkDepositType(depositType)) {
                valid = false;
                throw new Exception("depositType not valid");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return valid;
        }
    }

    private static boolean checkDepositType(String depositType) {
        for (DepositType depositTypeValue : DepositType.values()) {
            if (depositTypeValue.name().equalsIgnoreCase(depositType))
                return true;
        }
        return false;
    }


}
