package com.dotin.edu.common;

import com.dotin.edu.entity.*;

public class DepositFactory {
    public Deposit getDeposit(DepositType depositType, String customerNumber, Double depositBalance, int durationDays) {
        switch (depositType) {
            case LongTerm:
                return new LongTerm(customerNumber, depositBalance, durationDays);
            case ShortTerm:
                return new ShortTerm(customerNumber, depositBalance, durationDays);
            case Gharz:
                return new Gharz(customerNumber, depositBalance, durationDays);
            default:
                return null;

        }


    }
}
