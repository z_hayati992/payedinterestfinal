package com.dotin.edu.entity;

import com.dotin.edu.entity.Deposit;

public class Gharz extends Deposit {
    public Gharz() {
        super();
        this.interestRate = 0;
    }

    public Gharz(String customerNumber, Double depositBalance, int durationDays) {
        super(customerNumber, depositBalance, durationDays);
        this.interestRate = 0;
    }
}
