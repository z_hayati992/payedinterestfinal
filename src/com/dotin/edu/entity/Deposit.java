package com.dotin.edu.entity;

import com.dotin.edu.entity.Interest;

public class Deposit {
    private String customerNumber;
    private Double depositBalance;
    private int durationDays;
    protected int interestRate;
    private DepositType depositType;

    public Deposit() {
    }

    public Deposit(String customerNumber, Double depositBalance, int durationDays) {
        this.customerNumber = customerNumber;
        this.depositBalance = depositBalance;
        this.durationDays = durationDays;
    }


    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {

        this.depositType = depositType;
    }


    public int getInterestRate() {

        return interestRate;
    }

    public String getCustomerNumber() {

        return customerNumber;
    }

    public Deposit setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
        return this;
    }


    public Double getDepositBalance() {
        return depositBalance;
    }

    public Deposit setDepositBalance(Double depositBalance) {

        this.depositBalance = depositBalance;
        return this;
    }



    public int getDurationDays() {

        return durationDays;
    }

    public Deposit setDurationDays(int durationDays) {
        this.durationDays = durationDays;
        return this;
    }


}
