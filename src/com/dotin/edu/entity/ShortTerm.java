package com.dotin.edu.entity;

public class ShortTerm extends Deposit {

    public ShortTerm() {
        super();
        this.interestRate = 10;
    }

    public ShortTerm(String customerNumber, Double depositBalance, int durationDays) {
        super(customerNumber, depositBalance, durationDays);
        this.interestRate = 10;
    }


}
