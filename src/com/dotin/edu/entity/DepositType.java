package com.dotin.edu.entity;

public enum DepositType {
    LongTerm(2),
    ShortTerm(1),
    Gharz(0);
    private int value;

    DepositType(int value) {
        this.value = value;
    }

    public int getValue() {

        return value;
    }

    public static DepositType convert(String depositTypeValue) throws Exception {
        for (DepositType depositType : DepositType.values()) {
            if (depositType.name().equalsIgnoreCase(depositTypeValue))
                return depositType;

        }
        throw new Exception();

    }


}
