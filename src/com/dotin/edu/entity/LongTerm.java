package com.dotin.edu.entity;

public class LongTerm extends Deposit {
    public LongTerm() {
        super();
        this.interestRate = 20;
    }

    public LongTerm(String customerNumber, Double depositBalance, int durationDays) {
        super(customerNumber, depositBalance, durationDays);
        this.interestRate = 20;
    }
}